<?php

header('Content-Type: application/json');

require_once dirname(__DIR__) . "/vendor/autoload.php";

use PayrollService\Lib\DateTimeHelper;
use PayrollService\Lib\ExceptionThrower;
use PayrollService\Lib\DefaultLogger;
use PayrollService\Exception\GeneralException;
use PayrollService\Exception\PublicException;
use PayrollService\App;

$response = null;

try {
    require_once dirname(__DIR__) . "/bootstrap.php";

    $response = App::getResponse($_SERVER['REQUEST_URI']);

} catch (\Throwable $e) {

    if (!in_array(get_class($e), [GeneralException::class, PublicException::class])) {
        DefaultLogger::error($e->getMessage());
    }

    $response = [
        'body' => null,
        'error' => get_class($e) === PublicException::class ? $e->getMessage() : ExceptionThrower::getMessage(),
    ];
}

if ($response['error'] === null) {
    DefaultLogger::info("SUCCESS: response ready at " . DateTimeHelper::getCurrentTimestamp());
}

echo json_encode($response);



