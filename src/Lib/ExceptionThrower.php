<?php

namespace PayrollService\Lib;

use PayrollService\Exception\GeneralException;
use PayrollService\Exception\PublicException;

class ExceptionThrower
{
    public static function general(string $prefix = null, string $suffix = null): void
    {
        throw new GeneralException(self::getMessage($prefix, $suffix));
    }

    public static function public(string $prefix = null, string $suffix = null): void
    {
        throw new PublicException(self::getMessage($prefix, $suffix));
    }



    public static function getMessage(string $prefix = null, string $suffix = null): string
    {
        $timestamp = ConfigLoader::getLogTimestamp();
        return "{$prefix}Please report the following system error code to our support team for assistance: #$timestamp. Your cooperation is greatly appreciated!$suffix";
    }
}