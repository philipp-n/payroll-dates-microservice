<?php

namespace PayrollService\Lib;

class ConfigLoader
{
    private static ?string $logTimestamp = null;
    private static string $mainConfigJson;
    private static ?array $publicHolidays;

    public static function setLogTimestamp(string $logTimestamp = null): self
    {
        self::$logTimestamp = $logTimestamp ?? DateTimeHelper::getCurrentTimestamp();
        return new static();
    }

    public static function getLogTimestamp(): ?string
    {
        return self::$logTimestamp;
    }

    public static function setMainConfigFromFile(?string $path = null): self
    {
        $pathInit = $path;
        $defaultFilePath = dirname(__DIR__, 2) . '/config/main.json';
        $path = $path ?? $defaultFilePath;

        $mainConfigPath = is_file($path) ? $path : null;

        if ($mainConfigPath === null) {
            if ($pathInit === null) {
                DefaultLogger::critical("System cannot find default MainConfigFile using this path: $path");
            } else {
                DefaultLogger::error("Invalid MainConfigFile path provided: $path");
            }
            ExceptionThrower::general();
        }

        self::$mainConfigJson = file_get_contents($mainConfigPath);

        return new static();
    }

    public static function getMain(bool $asArray = false): \StdClass|array
    {
        $result = json_decode(self::$mainConfigJson, $asArray);
        if (!is_a($result, \StdClass::class) && !is_array($result)) {
            DefaultLogger::error("Invalid MainConfig JSON string: " . self::$mainConfigJson);
            ExceptionThrower::general();
        }
        return $result;
    }

    public static function setPublicHolidaysFromFile(?string $path = null): self
    {
        $pathInit = $path;
        $defaultFilePath = dirname(__DIR__, 2) . '/config/publicHolidays.php';
        $path = $path ?? $defaultFilePath;

        $publicHolidaysPath = is_file($path) ? $path : null;

        if ($publicHolidaysPath === null) {
            if ($pathInit === null) {
                DefaultLogger::critical("System cannot find default PublicHolidaysFile using this path: $path");
            } else {
                DefaultLogger::error("Invalid PublicHolidaysFile path provided: $path");
            }
            ExceptionThrower::general();
        }

        $publicHolidays = require $publicHolidaysPath;
        self::$publicHolidays = is_array($publicHolidays) ? $publicHolidays : null;

        return new static();
    }

    public static function getPublicHolidays(): array
    {
        if (self::$publicHolidays === null) {
            DefaultLogger::critical("No public holidays data provided");
            ExceptionThrower::general();
        }
        return self::$publicHolidays;
    }
}