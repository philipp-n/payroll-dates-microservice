<?php

namespace PayrollService\Lib;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Level;
use Monolog\Logger;

class DefaultLogger
{
    private static Logger $logger;

    public static function setLogger(Logger $logger = null): self
    {
        if ($logger === null) {
            $logger = new Logger('main');
            $logger->pushHandler(new RotatingFileHandler(dirname(__DIR__, 2) . '/log/main.log', 14, Level::Debug));
        }
        self::$logger = $logger;

        return new static();
    }

    public static function info(string|\Stringable $message = '', array $context = [], bool $displayRequestInfo = false)
    {
        self::$logger->info(self::getFullMessage($message, $displayRequestInfo, false), $context);
    }

    public static function warning(string|\Stringable $message, array $context = [])
    {
        self::$logger->warning(self::getFullMessage($message), $context);
    }

    public static function error(string|\Stringable $message, array $context = [])
    {
        self::$logger->error(self::getFullMessage($message), $context);
    }

    public static function critical(string|\Stringable $message, array $context = [])
    {
        self::$logger->critical(self::getFullMessage($message), $context);
    }

    private static function getFullMessage(string|\Stringable $message = '', bool $displayRequestInfo = false, bool $displayBacktrace = true): string
    {
        $result = "#" . ConfigLoader::getLogTimestamp() . ": ";

        if ($displayRequestInfo) {
            $result .= "{$_SERVER['REMOTE_ADDR']} '{$_SERVER['REQUEST_URI']}' ";
        }

        $result .= $message;

        if ($displayBacktrace) {
            $result .= " " . json_encode(self::getMinBacktrace());
        }

        return $result;
    }

    private static function getMinBacktrace(): array
    {
        $backtrace = array_reverse((array_filter(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS))));
        $backtrace = array_slice($backtrace, 0, -2);
        return array_map(
            function ($trace) {
                $file = $trace['file'] ?? null;
                if ($file) {
                    $file = substr($file, strlen(dirname(__DIR__, 2)));
                }
                $line = $trace['line'] ?? null;
                return $file ? "$file:$line" : '';
            },
            $backtrace
        );
    }
}
