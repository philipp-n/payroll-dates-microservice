<?php

namespace PayrollService\Lib;

use PayrollService\Lib\Enum\Direction;

class DateTimeHelper
{
    private static array $publicHolidays = [];

    public static function loadPublicHolidays(array $publicHolidays): void
    {
        self::$publicHolidays = $publicHolidays;
    }

    public static function findFirstSuitableWorkingDay(\DateTime $datetime, Direction $direction = Direction::FORWARD): \DateTime
    {
        $checkDate = clone $datetime;

        if (self::isWorkingDay($checkDate)) {
            return $checkDate;
        }

        $dayModifier = $direction === Direction::FORWARD ? '+1 day' : '-1 day';

        do {
            $checkDate->modify($dayModifier);
        } while (!self::isWorkingDay($checkDate));

        return $checkDate;
    }

    public static function modifyByWorkingDays(\DateTime $datetime, int $daysAmount, Direction $direction = Direction::FORWARD): \DateTime
    {
        $modifiedDate = clone $datetime;
        $daysMoved = 0;

        while ($daysMoved < abs($daysAmount)) {
            $modifiedDate->modify($direction === Direction::FORWARD ? '+1 day' : '-1 day');

            if (self::isWorkingDay($modifiedDate)) {
                $daysMoved++;
            }
        }

        return $modifiedDate;
    }

    private static function isWorkingDay(\DateTime $date): bool
    {
        $weekDay = (int)$date->format('N');
        $isWeekend = in_array($weekDay, [6, 7], true);

        $isStaticPublicHoliday = false;
        $isDynamicPublicHoliday = false;

        if(!empty(self::$publicHolidays)){
            $isStaticPublicHoliday = isset(self::$publicHolidays['staticDateHolidays']) && in_array($date->format('m-d'), self::$publicHolidays['staticDateHolidays'], true);
            $isDynamicPublicHoliday = isset(self::$publicHolidays['dynamicDateHolidays']) && in_array($date->format('Y-m-d'), self::$publicHolidays['dynamicDateHolidays'], true);
        }

        return !$isWeekend && !$isStaticPublicHoliday && !$isDynamicPublicHoliday;
    }

    public static function getCurrentTimestamp(bool $milliseconds = true){
        return (new \DateTime())->format($milliseconds ? "YmdHis.u" : "YmdHis");
    }
}