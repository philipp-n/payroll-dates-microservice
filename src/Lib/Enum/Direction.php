<?php

namespace PayrollService\Lib\Enum;

enum Direction: string
{
    case FORWARD = '+';
    case BACKWARD = '-';
}
