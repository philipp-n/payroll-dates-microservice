<?php

namespace PayrollService;

use PayrollService\Lib\ConfigLoader;
use PayrollService\Lib\DefaultLogger;
use PayrollService\Lib\ExceptionThrower;

class App
{
    public static function getResponse(string $requestUri): array
    {
        $requestRoute = trim(
            explode("?", $requestUri, 2)[0],
            '/'
        );
        list($yearInit) = explode('/', $requestRoute);

        $mainConfig = ConfigLoader::getMain();

        $year = filter_var($yearInit, FILTER_VALIDATE_INT, [
            'options' => [
                'default' => date('Y'),
                'min_range' => (int)$mainConfig->allowedYearFrom,
                'max_range' => (int)$mainConfig->allowedYearTo,
            ]
        ]);

        if (!empty($yearInit) && $year !== (int)$yearInit) {
            $msg = "Allowed year value range is {$mainConfig->allowedYearFrom} - {$mainConfig->allowedYearTo}.";
            DefaultLogger::error($msg);
            ExceptionThrower::public("$msg ");
        }

        $mainConfig = ConfigLoader::getMain();

        $calculator = new PayrollDatesCalculator(
            $mainConfig->paymentDayOfMonth,
            $mainConfig->advanceNoticePeriodInWorkdays,
            $mainConfig->dateTimeFormat,
        );

        return [
            'body' => $calculator->calculateForYear($year),
            'error' => null,
        ];
    }
}