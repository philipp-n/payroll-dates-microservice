<?php

namespace PayrollService;

use PayrollService\Lib\DateTimeHelper;
use PayrollService\Lib\Enum\Direction;

class PayrollDatesCalculator
{
    public function __construct(
        private readonly int    $paymentDayOfMonth,
        private readonly int    $advanceNoticePeriodInWorkdays,
        private readonly string $dateTimeFormat
    )
    {
    }

    public function calculateForYear(int $year): array
    {
        $result = [];

        $initialPaymentDate = \DateTime::createFromFormat("Ymd", "{$year}01{$this->paymentDayOfMonth}");

        foreach (range(1, 12) as $monthIndex) {
            $initialPaymentDate = $monthIndex === 1 ? $initialPaymentDate : clone $initialPaymentDate->modify("next month");

            $paymentDate = DateTimeHelper::findFirstSuitableWorkingDay($initialPaymentDate, Direction::BACKWARD);

            $notificationDate = DateTimeHelper::modifyByWorkingDays(
                $paymentDate, $this->advanceNoticePeriodInWorkdays, Direction::BACKWARD
            );

            $result[$paymentDate->format('F')] = [
                'notification' => $notificationDate->format($this->dateTimeFormat),
                'payment' => $paymentDate->format($this->dateTimeFormat)
            ];
        }

        return $result;
    }
}