# Payroll Service API

This Payroll Service API calculates payroll dates for a given year, taking into account weekends and public holidays to ensure payroll is processed on a workday. Additionally, it sends reminders to accountants `N` (configurable) working days before the payroll date.

---

## Requirements

- PHP 8.3 or higher
- Composer for managing PHP dependencies

---

## Setup

### Docker Setup

1. Clone the repository and navigate into the project directory:
   ```bash
   git clone https://gitlab.com/philipp-n/payroll-dates-microservice.git
   cd payroll-dates-microservice
   ```

2. Start the Docker containers:
   ```bash
   docker-compose up
   ```

3. Access the Docker container:
   ```bash
   docker-compose exec server bash
   ```

4. Install PHP dependencies:
   ```bash
   composer install
   ```

---

## Usage

Send a GET request to the API endpoint specifying the year:

`GET http://localhost:9000/<year>`

Where `<year>` is the year for which you want to calculate payroll dates. If `<year>` is not specified, the current year is used by default.

#### Examples:

- Request for the current year's payroll dates:  
  `GET http://localhost:9000`

- Request for payroll dates for the year 2020:  
  `GET http://localhost:9000/2020`

- Request for a year outside the allowed range (e.g., 2019 or 2028) results in an error:  
  `GET http://localhost:9000/2019`  
  `GET http://localhost:9000/2028`

### Response Format

The API responds with a JSON object containing payroll and reminder dates for each month of the specified year. Each month is represented as a key, with an object containing `notification` and `payment` dates as values.

#### Example Response:

```json
{
  "January": {
    "notification": "2024-01-05",
    "payment": "2024-01-10"
  },
  ...
  "December": {
    "notification": "2024-12-05",
    "payment": "2024-12-10"
  }
}
```

The `notification` date indicates when the reminder for payroll processing will be sent to the accountant, and the `payment` date is when the payroll will be disbursed. If a request is made for a year outside the allowed range, the response will contain an error message.

```json
{
  "body": null,
  "error": "Invalid year requested. Allowed year value range is 2020 - 2027. Please report the following system error code to our support team for assistance: #<timestamp> Your cooperation is greatly appreciated!"
}
```

---

## Configuration

Configure payroll settings in the `config/main.json` and public holidays in `config/publicHolidays.php`.

Example `main.json` configuration:

```json
{
"paymentDayOfMonth": 10,
"advanceNoticePeriodInWorkdays": 3,
"dateTimeFormat": "Y-m-d",
"allowedYearFrom": 2020,
"allowedYearTo": 2027
}
```

Public holidays should be configured in `publicHolidays.php` with the date format `Y-m-d` for dynamic dates or `m-d` for recurring dates.

---

## Testing

Run the PHPUnit tests to ensure everything is working correctly:

```bash
./vendor/bin/phpunit
```

---

## Security

This project does not directly handle authentication or sensitive user data.

---

## Logging

The Payroll Service API utilizes Monolog for logging. By default, log messages are written to `log/main-*-*-*.log` within the project directory. The logger records information about API requests, errors, and system operations critical for debugging and monitoring the application's health.

### Log Levels

The application logs messages across different levels:

- **INFO**: Successful operations, such as payroll calculations and reminders sent.
- **ERROR**: Runtime errors or unexpected conditions. Exception details and stack traces are included for debugging.
- **CRITICAL**: Severe conditions, such as system component failures that require immediate attention.

### Viewing Logs

To view the application logs, you can simply open the `log/main-*-*-*.log` file:

```bash
log/main-<year>-<month>-<day>.log
```

### Configuring Logging

The default logging configuration is set up in `src/Lib/DefaultLogger.php`. You can customize logger settings, such as the log file path or log level, by modifying this file. Additionally, you can integrate other handlers supported by Monolog to log messages to external systems or services for centralized logging.

---

## License

This project is open-sourced under the MIT License.

---

# Places for Improvement

## Extensive Testing

While the current test suite covers the basic functionality of the payroll date calculation service, there is always room for improvement in testing to ensure higher reliability and maintainability. Future enhancements could include:

- **Integration tests** to verify the service's interaction with external systems (if any) and its performance under more realistic scenarios.
- **Performance tests** to ensure that the service can handle a high volume of requests without significant degradation in response times.
- **Security tests** to identify and mitigate potential vulnerabilities, ensuring the protection of sensitive data.

Expanding the test coverage will help catch bugs early, reduce the risk of regressions, and improve the overall quality of the software.

## Caching Implementation

Currently, the service calculates payroll dates dynamically for each request, which might be inefficient, especially for common queries. Implementing caching could significantly improve the service's efficiency and response times by storing frequently accessed or computationally intensive results. Considerations for caching include:

- **Selective Caching:** Identifying which results are worth caching based on their access patterns and computational complexity.
- **Cache Invalidation:** Determining when and how cached data should be invalidated to ensure the accuracy of the information provided.
- **Scalable Caching Strategy:** Using distributed caching solutions like Redis or Memcached to support scalability and high availability.

Caching could be initially implemented for the most common year queries and then expanded based on demand and access patterns observed in production.

## Continuous Integration and Continuous Deployment (CI/CD)

Implementing CI/CD pipelines can streamline the process of integrating new code changes, running tests, and deploying updates to production. This would ensure that:

- Code changes are automatically tested, reducing the risk of introducing regressions.
- Deployments are automated and can be performed with minimal downtime.
- The development process is more agile, allowing for faster iterations and feedback cycles.

Setting up CI/CD with tools like Jenkins, GitLab CI/CD, or GitHub Actions would enhance operational efficiency and the overall agility of the development process.

## Accessibility and Internationalization

Considering the potential for the service to be used in different regions, implementing internationalization (i18n) and ensuring accessibility standards are met could broaden the user base and improve user experience. This includes:

- Providing translations for different languages.
- Ensuring the service's API documentation and error messages are clear and understandable across different cultures.
- Adhering to accessibility guidelines to accommodate users with disabilities.

These improvements would make the service more inclusive and usable for a wider audience.

---

These areas for improvement are intended as a roadmap for future development efforts, prioritizing enhancements that align with user needs and business objectives.

