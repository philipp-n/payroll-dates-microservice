<?php

use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;
use PayrollService\Lib\ConfigLoader;
use PayrollService\Lib\DateTimeHelper;
use PayrollService\Lib\DefaultLogger;

ConfigLoader
    ::setLogTimestamp()
    ::setMainConfigFromFile(__DIR__ . "/Config/main.json")
    ::setPublicHolidaysFromFile(__DIR__ . "/Config/publicHolidays.php");
DateTimeHelper::loadPublicHolidays(ConfigLoader::getPublicHolidays());

$logger = new Logger('main');
$logger->pushHandler(new StreamHandler(__DIR__. '/log/main.log', Level::Debug));
DefaultLogger::setLogger($logger);