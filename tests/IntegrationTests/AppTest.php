<?php

namespace Tests\IntegrationTests;

use PayrollService\App;
use PayrollService\Exception\GeneralException;
use PayrollService\Exception\PublicException;
use PayrollService\Lib\ConfigLoader;
use PHPUnit\Framework\TestCase;

class AppTest extends TestCase
{
    private string $testLogPath;

    public function __construct()
    {
        parent::__construct();
        $this->testLogPath = dirname(__DIR__) . "/log/main.log";
    }

    public function testRegular()
    {
        $response = App::getResponse('/2024');

        $this->assertEquals(
            file_get_contents(dirname(__DIR__) . "/ExpectedData/2024.custom.json"),
            json_encode($response)
        );
    }

    public function testYearOutOfRange()
    {
        file_put_contents($this->testLogPath, '');
        try {
            App::getResponse('/2019');
            $this->fail("Expected PublicException was not thrown.");
        } catch (PublicException $e) {
            $this->assertMatchesRegularExpression(
                "/Allowed year value range is 2020 - 2027. Please report the following system error code to our support team for assistance: #.*. Your cooperation is greatly appreciated!/",
                $e->getMessage()
            );
            $this->assertMatchesRegularExpression(
                "/\[.*] main.ERROR: #" . date("Ymd") . ".*: Allowed year value range is 2020 - 2027\..*/",
                file_get_contents($this->testLogPath)
            );
        }
    }

    public function testWrongConfigPath_main()
    {
        file_put_contents($this->testLogPath, '');
        try {
            ConfigLoader
                ::setMainConfigFromFile(__DIR__ . "/invalid/path/Config/main.json");

            App::getResponse('/2024');
            $this->fail("Expected PublicException was not thrown.");
        } catch (GeneralException $e) {
            $this->assertMatchesRegularExpression(
                "/Please report the following system error code to our support team for assistance: #.* Your cooperation is greatly appreciated!/",
                $e->getMessage()
            );
            $this->assertMatchesRegularExpression(
                "/\\[.*] main\\.ERROR: #" . date("Ymd") . ".*: Invalid MainConfigFile path provided:.*tests\\/IntegrationTests\\/invalid\\/path\\/Config\\/main\\.json.*/",
                file_get_contents($this->testLogPath)
            );
        }
    }

    public function testWrongConfigPath_publicHolidays()
    {
        file_put_contents($this->testLogPath, '');
        try {
            ConfigLoader
                ::setPublicHolidaysFromFile(__DIR__ . "/invalid/path/Config/publicHolidays.php");

            App::getResponse('/2024');
            $this->fail("Expected PublicException was not thrown.");
        } catch (GeneralException $e) {
            $this->assertMatchesRegularExpression(
                "/Please report the following system error code to our support team for assistance: #.* Your cooperation is greatly appreciated!/",
                $e->getMessage()
            );
            $this->assertMatchesRegularExpression(
                "/\\[.*] main\\.ERROR: #" . date("Ymd") . ".*: Invalid PublicHolidaysFile path provided:.*tests\\/IntegrationTests\\/invalid\\/path\\/Config\\/publicHolidays\\.php.*/",
                file_get_contents($this->testLogPath)
            );
        }
    }
}
