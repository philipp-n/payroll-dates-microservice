<?php

namespace Tests\IntegrationTests;

use PHPUnit\Framework\TestCase;

/**
 * NB! This testcase write entries directly to '/log/main-*-*-*.log'. Never run in PRODUCTION
 */
class EndpointTest extends TestCase
{
    private $baseUrl = 'http://localhost';

    private function skipIfRequired(bool $checkMainConfigOnly = false)
    {
        $configDir = dirname(__DIR__, 2) . "/config";

        if (hash_file('sha256', "$configDir/main.json") !== "3953f65360f2c0ea3d1a923b4fc5f8bc431b1475d4fe2dfeb1ee571844968245") {
            $this->markTestSkipped("Initial main config data have changed");
        }

        if ($checkMainConfigOnly) {
            return;
        }

        if (hash_file('sha256', "$configDir/publicHolidays.php") !== "d5c01eecac817557c534f3bda0399e9d3f527be93abd8b74199a06009fcd1517") {
            $this->markTestSkipped("Initial publicHolidays config data have changed");
        }

        if ((int)date("Y") !== 2024) {
            $this->markTestSkipped("Initial year context has changed");
        }
    }

    public function testEndpoint_noYear()
    {
        $this->skipIfRequired();
        $this->assertEquals(
            file_get_contents(dirname(__DIR__) . "/ExpectedData/2024.default.json"),
            file_get_contents($this->baseUrl)
        );
    }

    public function testEndpoint_2020()
    {
        $this->skipIfRequired();
        $this->assertEquals(
            file_get_contents(dirname(__DIR__) . "/ExpectedData/2020.json"),
            file_get_contents($this->baseUrl . "/2020")
        );
    }

    public function testEndpoint_2019_notInRange()
    {
        $this->skipIfRequired(true);

        $response = json_decode(file_get_contents($this->baseUrl . "/2019"));

        $this->assertNull($response->body);
        $this->assertMatchesRegularExpression(
            "/Allowed year value range is 2020 - 2027. Please report the following system error code to our support team for assistance: #" . date("Ymd") .
            ".*\. Your cooperation is greatly appreciated!/",
            $response->error);
    }
}
