<?php

namespace Tests\UnitTests\DateTimeHelper;

use PayrollService\Lib\DateTimeHelper;
use PHPUnit\Framework\TestCase;

class IsWorkingDayTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        DateTimeHelper::loadPublicHolidays([]);
    }

    public function testIsWorkingDay_monday_noData()
    {
        $datetimeHelperReflection = new \ReflectionClass(DateTimeHelper::class);
        $method = $datetimeHelperReflection->getMethod('isWorkingDay');
        $method->setAccessible(true);

        $date = new \DateTime('2024-01-01');
        $result = $method->invokeArgs(null, [$date]);
        $this->assertTrue($result);
    }

    public function testIsWorkingDay_monday_notInData()
    {
        DateTimeHelper::loadPublicHolidays(['staticDateHolidays' => ['02-24']]);

        $datetimeHelperReflection = new \ReflectionClass(DateTimeHelper::class);
        $method = $datetimeHelperReflection->getMethod('isWorkingDay');
        $method->setAccessible(true);

        $date = new \DateTime('2024-01-01');
        $result = $method->invokeArgs(null, [$date]);
        $this->assertTrue($result);
    }

    public function testIsWorkingDay_monday_inData_static()
    {
        DateTimeHelper::loadPublicHolidays(['staticDateHolidays' => ['01-01']]); // New Year's Day

        $datetimeHelperReflection = new \ReflectionClass(DateTimeHelper::class);
        $method = $datetimeHelperReflection->getMethod('isWorkingDay');
        $method->setAccessible(true);

        $date = new \DateTime('2024-01-01');
        $result = $method->invokeArgs(null, [$date]);
        $this->assertFalse($result);
    }

    public function testIsWorkingDay_monday_inData_dynamic()
    {
        DateTimeHelper::loadPublicHolidays(['dynamicDateHolidays' => ['2024-01-01']]); // New Year's Day (set as dynamical for testing)

        $datetimeHelperReflection = new \ReflectionClass(DateTimeHelper::class);
        $method = $datetimeHelperReflection->getMethod('isWorkingDay');
        $method->setAccessible(true);

        $date = new \DateTime('2024-01-01');
        $result = $method->invokeArgs(null, [$date]);
        $this->assertFalse($result);
    }

    public function testIsWorkingDay_sunday_noData()
    {
        $datetimeHelperReflection = new \ReflectionClass(DateTimeHelper::class);
        $method = $datetimeHelperReflection->getMethod('isWorkingDay');
        $method->setAccessible(true);

        $date = new \DateTime('2023-12-31');
        $result = $method->invokeArgs(null, [$date]);
        $this->assertFalse($result);
    }
}
