<?php

namespace Tests\UnitTests\DateTimeHelper;

use PayrollService\Lib\DateTimeHelper;
use PayrollService\Lib\Enum\Direction;
use PHPUnit\Framework\TestCase;

class ModifyByWorkingDaysTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        DateTimeHelper::loadPublicHolidays([]);
    }

    public function testIsWorkingDay_sunday_noData_forward()
    {
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::modifyByWorkingDays($date, 3)->format("Y-m-d"),
            (new \DateTime('2024-01-03'))->format("Y-m-d")
        );
    }

    public function testIsWorkingDay_sunday_data_forward_static()
    {
        DateTimeHelper::loadPublicHolidays(['staticDateHolidays' => ['01-03']]);
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::modifyByWorkingDays($date, 3)->format("Y-m-d"),
            (new \DateTime('2024-01-04'))->format("Y-m-d")
        );
    }

    public function testIsWorkingDay_sunday_data_forward_dynamic()
    {
        DateTimeHelper::loadPublicHolidays(['dynamicDateHolidays' => ['2024-01-03']]);
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::modifyByWorkingDays($date, 3)->format("Y-m-d"),
            (new \DateTime('2024-01-04'))->format("Y-m-d")
        );
    }

    public function testIsWorkingDay_sunday_noData_backward()
    {
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::modifyByWorkingDays($date, 3, Direction::BACKWARD)->format("Y-m-d"),
            (new \DateTime('2023-12-27'))->format("Y-m-d")
        );
    }

    public function testIsWorkingDay_sunday_data_backward_dynamic()
    {
        DateTimeHelper::loadPublicHolidays(['dynamicDateHolidays' => ['2023-12-25', '2023-12-26', '2023-12-27']]);
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::modifyByWorkingDays($date, 3, Direction::BACKWARD)->format("Y-m-d"),
            (new \DateTime('2023-12-22'))->format("Y-m-d")
        );
    }

    public function testIsWorkingDay_sunday_data_backward_static()
    {
        DateTimeHelper::loadPublicHolidays(['staticDateHolidays' => ['12-25', '12-26', '12-27']]);
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::modifyByWorkingDays($date, 3, Direction::BACKWARD)->format("Y-m-d"),
            (new \DateTime('2023-12-22'))->format("Y-m-d")
        );
    }

    public function testIsWorkingDay_sunday_data_5days_backward()
    {
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::modifyByWorkingDays($date, 5, Direction::BACKWARD)->format("Y-m-d"),
            (new \DateTime('2023-12-25'))->format("Y-m-d")
        );
    }

    public function testIsWorkingDay_sunday_data_6days_forward()
    {
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::modifyByWorkingDays($date, 6)->format("Y-m-d"),
            (new \DateTime('2024-01-08'))->format("Y-m-d")
        );
    }
}
