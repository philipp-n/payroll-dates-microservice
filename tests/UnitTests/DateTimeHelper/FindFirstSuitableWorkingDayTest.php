<?php

namespace Tests\UnitTests\DateTimeHelper;

use PayrollService\Lib\DateTimeHelper;
use PayrollService\Lib\Enum\Direction;
use PHPUnit\Framework\TestCase;

class FindFirstSuitableWorkingDayTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        DateTimeHelper::loadPublicHolidays([]);
    }

    public function testIsWorkingDay_sunday_noData_forward()
    {
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::findFirstSuitableWorkingDay($date)->format("Y-m-d"),
            (new \DateTime('2024-01-01'))->format("Y-m-d")
        );
    }

    public function testIsWorkingDay_sunday_data_forward_dynamic()
    {
        DateTimeHelper::loadPublicHolidays(['dynamicDateHolidays' => ['2024-01-01']]);
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::findFirstSuitableWorkingDay($date)->format("Y-m-d"),
            (new \DateTime('2024-01-02'))->format("Y-m-d")
        );
    }

    public function testIsWorkingDay_sunday_noData_backward()
    {
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::findFirstSuitableWorkingDay($date, Direction::BACKWARD)->format("Y-m-d"),
            (new \DateTime('2023-12-29'))->format("Y-m-d")
        );
    }

    public function testIsWorkingDay_sunday_data_backward_static()
    {
        DateTimeHelper::loadPublicHolidays(['staticDateHolidays' => ['12-29']]);
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::findFirstSuitableWorkingDay($date, Direction::BACKWARD)->format("Y-m-d"),
            (new \DateTime('2023-12-28'))->format("Y-m-d")
        );
    }

    public function testIsWorkingDay_sunday_data_backward_dynamic()
    {
        DateTimeHelper::loadPublicHolidays(['dynamicDateHolidays' => ['2023-12-29']]);
        $date = new \DateTime('2023-12-31');
        $this->assertEquals(
            DateTimeHelper::findFirstSuitableWorkingDay($date, Direction::BACKWARD)->format("Y-m-d"),
            (new \DateTime('2023-12-28'))->format("Y-m-d")
        );
    }
}
