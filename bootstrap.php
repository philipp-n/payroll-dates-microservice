<?php

use PayrollService\Lib\ConfigLoader;
use PayrollService\Lib\DefaultLogger;
use PayrollService\Lib\DateTimeHelper;

ConfigLoader::setLogTimestamp();
DefaultLogger::setLogger()::info(message: "New request", displayRequestInfo: true);
ConfigLoader::setMainConfigFromFile()::setPublicHolidaysFromFile();
DateTimeHelper::loadPublicHolidays(ConfigLoader::getPublicHolidays());