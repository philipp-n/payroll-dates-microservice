<?php

return [
    'staticDateHolidays' => [
        "01-01", // New Year's Day
        "02-24", // Independence Day
        "06-23", // Victory Day
        "06-24", // Midsummer Day
        "08-20", // Day of Restoration of Independence
        "12-24", // Christmas Eve
        "12-25", // Christmas Day
        "12-26", // Boxing Day
        "12-31", // New Year's Eve
    ],
    'dynamicDateHolidays' => [
        "2020-04-10", // Good Friday 2020
        "2020-04-12", // Easter Sunday 2020
        "2021-04-02", // Good Friday 2021
        "2021-04-04", // Easter Sunday 2021
        "2022-04-15", // Good Friday 2022
        "2022-04-17", // Easter Sunday 2022
        "2023-04-07", // Good Friday 2023
        "2023-04-09", // Easter Sunday 2023
        "2024-03-29", // Good Friday 2024
        "2024-03-31", // Easter Sunday 2024
        "2025-04-18", // Good Friday 2025
        "2025-04-20", // Easter Sunday 2025
        "2026-04-03", // Good Friday 2026
        "2026-04-05", // Easter Sunday 2026
        "2027-03-26", // Good Friday 2027
        "2027-03-28"  // Easter Sunday 2027
    ]
];